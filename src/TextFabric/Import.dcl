definition module TextFabric.Import

/**
 * This file is part of text-fabric.
 *
 * Text-fabric is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Text-fabric is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with text-fabric. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError
from TextFabric import :: DataSet, :: DataSet`, :: EdgeSet

/**
 * @param Callback which is called before every file is opened.
 * @param Feature names to import.
 * @param Path to the `tf` directory.
 */
import_tf :: !(String *World -> *World) ![String] !String !*World -> (!MaybeError String DataSet, !*World)

:: FeatureType = NodeFeature | EdgeFeature | ConfigFile

/**
 * Find out what features are in a dataset.
 * @param Path to the `tf` directory.
 */
get_features :: !String !*World -> (!MaybeError String [(String,FeatureType)], !*World)

//* Only exported for use in TextFabric.NodeMapping.
skip_header :: !*File -> *File

//* Only exported for use in TextFabric.NodeMapping.
parse_nodes :: !String -> MaybeError String [Int]

//* Only exported for use in TextFabric.NodeMapping.
trim_line_endings :: !String -> String
