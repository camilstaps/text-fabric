implementation module TextFabric.Import

/**
 * This file is part of text-fabric.
 *
 * Text-fabric is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Text-fabric is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with text-fabric. If not, see <https://www.gnu.org/licenses/>.
 */

import Control.Monad
import Data.Either
import Data.Error
from Data.Foldable import class Foldable(foldl`()), instance Foldable []
from Data.Func import mapSt
import Data.Functor
import qualified Data.IntMap.Base
import qualified Data.IntMap.Strict
import Data.Maybe
import Data.OrdList
import qualified Data.Set
import StdEnv
import StdOverloadedList
import System.Directory
import System.FilePath
import System._Unsafe
from Text import class Text(split,trim), instance Text String

import TextFabric

// NB: it must be possible to unbox this
:: TempEdgesList =
	{ edges :: ![#Int!]
	}

import_tf :: !(String *World -> *World) ![String] !String !*World -> (!MaybeError String DataSet, !*World)
import_tf callback feature_names dir w
	# feature_names = ["otype":removeDup (filter ((<>)"otype") feature_names)]

	# otype_filename = dir </> "otype.tf"
	# w = callback otype_filename w
	# (ok,otype,w) = fopen otype_filename FReadText w
	| not ok = (Error ("Failed to open "+++otype_filename),w)
	# (otypes,otype) = import_otype_file otype
	# (_,w) = fclose otype w
	| isError otypes = (liftError otypes,w)
	# otypes = fromOk otypes

	# nnodes = size otypes
	# dataset =
		{ node_features = {name \\ name <- feature_names}
		, edge_features = {"oslots"}
		, nodes =
			{
				{ features = {createArray (length feature_names) "" & [0]=type}
				, outgoing = {{edges=[|]}}
				, incoming = {{edges=[|]}}
				}
			\\ type <-: otypes
			}
		}

	# oslots_filename = dir </> "oslots.tf"
	# w = callback oslots_filename w
	# (ok,oslot,w) = fopen oslots_filename FReadText w
	| not ok = (Error ("Failed to open "+++oslots_filename), w)
	# (dataset,oslot) = import_edge_file 0 dataset oslot
	# (_,w) = fclose oslot w
	| isError dataset = (liftError dataset,w)
	# dataset = fromOk dataset

	# dataset = optimise_edges nnodes dataset

	# (dataset,w) = foldlError
		(\dataset w (fi,feature)
			# feature_filename = dir </> feature+++".tf"
			# w = callback feature_filename w
			# (ok,f,w) = fopen feature_filename FReadText w
			| not ok -> (Error ("Failed to open "+++feature_filename), w)
			# (dataset,f) = import_node_file fi dataset f // TODO support edge files
			# (_,w) = fclose f w
			-> (dataset, w))
		dataset w
		[(i,name) \\ i <- [1..] & name <- tl feature_names]
	| isError dataset = (liftError dataset,w)
	# dataset = fromOk dataset

	= (Ok dataset,w)
where
	foldlError op st w [] = (Ok st,w)
	foldlError op st w [x:xs] = case op st w x of
		(Ok x,w) -> foldlError op x w xs
		e        -> e

instance < {32#a} | Array {32#} a & < a
where
	(<) xs ys
		| size xs < size ys
			= True
		| size ys < size xs
			= False
			= cmp (size xs-1)
	where
		cmp -1 = False
		cmp i
			| xs.[i] < ys.[i]
				= True
			| ys.[i] < xs.[i]
				= False
				= cmp (i-1)

optimise_edges :: !Int !*(DataSet` TempEdgesList) -> .DataSet
optimise_edges nnodes {node_features,edge_features,nodes}
	# edge_sets = 'Data.Set'.fromList (flatten [flatten [[to_array e \\ e <-: n.outgoing], [to_array e \\ e <-: n.incoming]] \\ n <-: nodes])
	= unsafeCoerce
		{ node_features = node_features
		, edge_features = edge_features
		, nodes =
			{
				{ features = n.features
				, outgoing = {fromJust ('Data.Set'.getMember (to_array set) edge_sets) \\ set <-: n.outgoing}
				, incoming = {fromJust ('Data.Set'.getMember (to_array set) edge_sets) \\ set <-: n.incoming}
				}
			\\ n <-: nodes
			}
		}
where
	to_array {edges}
		/* NB: we *have* to create a node for the sorted list ourselves;
		 * otherwise the array comprehension will evaluate it twice! */
		#! edges = Sort [e \\ e <|- edges]
		= {32# e \\ e <- edges}

import_otype_file :: !*File -> (!MaybeError String .{#String}, !*File)
import_otype_file f
	# f = skip_header f
	# (node_map,f) = import_nodes 0 'Data.IntMap.Strict'.newMap f
	| isError node_map = (liftError node_map, f)
	# node_map = fromOk node_map
	# nodes = createArray (last ('Data.IntMap.Strict'.keys node_map)+1) ""
	/* NB: we *have* to create a node for the sorted list ourselves; otherwise
	 * the array comprehension will evaluate it twice! */
	#! node_map = 'Data.IntMap.Strict'.toList node_map
	= (Ok {nodes & [i]=v \\ (i,v) <- node_map}, f)
where
	// TODO: unescape
	import_nodes :: !Int !('Data.IntMap.Base'.IntMap String) !*File
		-> (!MaybeError String ('Data.IntMap.Base'.IntMap String), !*File)
	import_nodes i nodes f
		# (line,f) = freadline f
		| size line == 0 = (Ok nodes, f)
		| otherwise = case split "\t" (trim_line_endings line) of
			[val]
				-> import_nodes (i+1) ('Data.IntMap.Strict'.put i val nodes) f
			[ns,val]
				# ns = parse_nodes ns
				| isError ns
					-> (liftError ns, f)
					# ns = fromOk ns
					-> import_nodes (maxList ns+1) (foldl (\m n -> 'Data.IntMap.Strict'.put n val m) nodes ns) f
			_
				-> (Error "node line with too many values", f)

import_node_file :: !Int !*DataSet !*File -> (!MaybeError String .DataSet, !*File)
import_node_file feature_id data f
	# f = skip_header f
	= import_nodes 'Data.Set'.newSet 0 data f
where
	// TODO: unescape
	import_nodes :: !('Data.Set'.Set String) !Int !*DataSet !*File -> (!MaybeError String .DataSet, !*File)
	import_nodes vals i data f
		# (line,f) = freadline f
		| size line == 0 = (Ok data, f)
		| otherwise = case split "\t" (trim_line_endings line) of
			[val]
				| size val == 0
					-> import_nodes vals (i+1) {data & nodes.[i].features.[feature_id]=""} f
				# oldval = 'Data.Set'.getMember val vals
				| oldval =: ?None
					-> import_nodes ('Data.Set'.insert val vals) (i+1) {data & nodes.[i].features.[feature_id]=val} f
					# (?Just val) = oldval
					-> import_nodes vals (i+1) {data & nodes.[i].features.[feature_id]=val} f
			[ns,val]
				#! ns = parse_nodes ns
				| isError ns
					-> (liftError ns, f)
					# ns = fromOk ns
					# next = maxList ns+1
					| size val == 0
						-> import_nodes vals next {data & nodes.[i].features.[feature_id]="" \\ i <- ns} f
					# oldval = 'Data.Set'.getMember val vals
					| oldval =: ?None
						-> import_nodes ('Data.Set'.insert val vals) next {data & nodes.[i].features.[feature_id]=val \\ i <- ns} f
						# (?Just val) = oldval
						-> import_nodes vals next {data & nodes.[i].features.[feature_id]=val \\ i <- ns} f
			_
				-> (Error "node line with too many values", f)

// TODO: this currently assumes there are no edge values
import_edge_file :: !Int !*(DataSet` TempEdgesList) !*File -> (!MaybeError String .(DataSet` TempEdgesList), !*File)
import_edge_file edge_id data f
	# f = skip_header f
	= import_edges 0 data f
where
	import_edges :: !Int !*(DataSet` TempEdgesList) !*File -> (!MaybeError String .(DataSet` TempEdgesList), !*File)
	import_edges i data f
		# (line,f) = freadline f
		| size line == 0 = (Ok data, f)
		| otherwise = case split "\t" (trim_line_endings line) of
			[to]
				# to = parse_nodes to
				| isError to -> (liftError to, f)
				# to = fromOk to
				# (edge_set,data) = data!nodes.[i].outgoing.[edge_id]
				-> import_edges
					(i+1)
					(foldl`
						(\data n
							# (edge_set,data) = data!nodes.[n].incoming.[edge_id]
							-> {data & nodes.[n].incoming.[edge_id]=add [|i] edge_set})
						{data & nodes.[i].outgoing.[edge_id]=add [|n \\ n <- to] edge_set}
						to)
					f
			[fr,to]
				#! fr = parse_nodes fr
				| isError fr -> (liftError fr, f)
				#! fr = fromOk fr
				#! to = parse_nodes to
				| isError to -> (liftError to, f)
				#! to = fromOk to
				-> import_edges
					(maxList fr+1)
					(foldl`
						(\data n
							# (edge_set,data) = data!nodes.[n].incoming.[edge_id]
							-> {data & nodes.[n].incoming.[edge_id]=add [|i \\ i <- fr] edge_set})
						(foldl`
							(\data n
								# (edge_set,data) = data!nodes.[n].outgoing.[edge_id]
								-> {data & nodes.[n].outgoing.[edge_id]=add [|i \\ i <- fr] edge_set})
							data
							fr)
						to)
					f
			_
				-> (Error "edge line with too many values", f)
	where
		add xs {edges} = {edges=xs ++| edges}

skip_header :: !*File -> *File
skip_header f
	# (line,f) = freadline f
	# line = trim_line_endings line
	| size line == 0
		= f
	| line.[0] == '@'
		= skip_header f
		= f

parse_nodes :: !String -> MaybeError String [Int]
parse_nodes s
	# parsed = map parse (split "," s)
	| any isError parsed
		= liftError (hd (filter isError parsed))
		= Ok [i-1 \\ Ok is <- parsed, i <- is]
		// NB: we use i-1 and not just i here because the data format is 1-indexed
where
	parse s = case [toInt i \\ i <- split "-" s] of
		[i]   -> Ok [i]
		[i,j] -> Ok (if (i<j) [i..j] [j..i])
		_     -> Error "parse_nodes: zero or more than two values in range"

get_features :: !String !*World -> (!MaybeError String [(String,FeatureType)], !*World)
get_features path w
	# (paths,w) = readDirectory path w
	| isError paths
		= (Error (snd (fromError paths)), w)
	# (features,w) = mapSt get_feature (fromOk paths) w
	| any isError features
		= (liftError (hd (filter isError features)), w)
		= (Ok [f \\ Ok (?Just f) <- features], w)
where
	get_feature :: !String !*World -> (!MaybeError String (?(String,FeatureType)), !*World)
	get_feature feature_path w
		| takeExtension feature_path <> "tf"
			= (Ok ?None, w)
		# (ok,f,w) = fopen (path </> feature_path) FReadText w
		| not ok
			= (Error ("Failed to open "+++feature_path), w)
		# (line,f) = freadline f
		# (ok,w) = fclose f w
		| not ok
			= (Error ("Failed to close "+++feature_path), w)
		# line = trim line
		| line == "@node"
			= (Ok (?Just (name, NodeFeature)), w)
		| line == "@edge"
			= (Ok (?Just (name, EdgeFeature)), w)
		| line == "@config"
			= (Ok (?Just (name, ConfigFile)), w)
			= (Error ("Unknown feature type "+++line), w)
	where
		name = dropExtension feature_path

trim_line_endings :: !String -> String
trim_line_endings s
	# end = find_last_char (size s-1)
	| end == size s-1
		= s
		= s % (0,end)
where
	find_last_char -1 = -1
	find_last_char i
		| s.[i]=='\n' || s.[i]=='\r'
			= find_last_char (i-1)
			= i
