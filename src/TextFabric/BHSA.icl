implementation module TextFabric.BHSA

/**
 * This file is part of text-fabric.
 *
 * Text-fabric is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Text-fabric is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with text-fabric. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

from Data.Func import $
from Text import class Text(concat), instance Text String

import TextFabric
import TextFabric.Filters

get_text :: !NodeRef !DataSet -> [(?NodeRef,String)]
get_text node data = case get_node_feature_ids ["g_word_utf8","trailer_utf8"] data of
	?Just [g_word_utf8,trailer_utf8] ->
		flatten
			[ let word = data.nodes.[ref] in
				[ (?Just ref, get_node_feature g_word_utf8 word)
				, (?None,     get_node_feature trailer_utf8 word)
				]
			\\ ref <|- words ]
	_ -> []
where
	words
		| data.nodes.[node].features.[0] == "word"
			= [|node]
			= get_child_node_refs_with (isOfType "word") data.nodes.[node] data
