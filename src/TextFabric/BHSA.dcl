definition module TextFabric.BHSA

/**
 * This file is part of text-fabric.
 *
 * Text-fabric is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Text-fabric is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with text-fabric. If not, see <https://www.gnu.org/licenses/>.
 */

from TextFabric import :: DataSet, :: DataSet`, :: Node, :: Node`, :: NodeRef, :: EdgeSet

get_text :: !NodeRef !DataSet -> [(?NodeRef,String)]
