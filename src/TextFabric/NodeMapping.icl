implementation module TextFabric.NodeMapping

/**
 * This file is part of text-fabric.
 *
 * Text-fabric is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Text-fabric is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with text-fabric. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
from Data.Map import :: Map
import qualified Data.Map
import Data.Maybe
from Text import class Text(split), instance Text String

import TextFabric
import TextFabric.Import

parse_node_mapping_file :: !*File -> (!MaybeError String NodeMapping, !*File)
parse_node_mapping_file f
	# f = skip_header f
	= import_edges 0 'Data.Map'.newMap f
where
	import_edges :: !Int !NodeMapping !*File -> (!MaybeError String NodeMapping, !*File)
	import_edges i mapping f
		# (line,f) = freadline f
		// freadline gives "" on eof
		| size line == 0 =
			(Ok mapping, f)
		// There can be up to three parts in each line:
		// [t] means there is an edge from i to t
		// [t,d] means there is an edge from i to t with dissimilarity d (the edge value)
		// [f,t,d] means there is an edge from f to t with dissimilarity d
		# parts = split "\t" (trim_line_endings line)
		| isEmpty parts =
			(Error "empty edge line", f)
		| parts=:[_,_,_,_:_] =
			(Error "edge line with too many parts", f)
		# fr = case parts of [fr,_,_] -> parse_nodes fr; _ -> Ok [i]
		| isError fr =
			(liftError fr, f)
		# fr = fromOk fr
		# to = case parts of [_,to,_] -> parse_nodes to; [to:_] -> parse_nodes to
		| isError to =
			(liftError to, f)
		# to = fromOk to
		# dissimilarity = case parts of [_] -> ?None; _ -> ?Just (toInt (last parts))
		| dissimilarity=:(?Just 0) && last parts <> "0" =
			(Error "failed to parse dissimilarity as integer", f)
		// Update the mapping for each from-to pair
		# mapping = foldr (\(f,t) -> 'Data.Map'.alter (add dissimilarity t) f) mapping [(f,t) \\ f <- fr, t <- to]
		= import_edges (maxList fr+1) mapping f
	where
		add dissimilarity target mbOtherTargets = ?Just
			[|{target=target, dissimilarity=dissimilarity}
			: fromMaybe [|] mbOtherTargets
			]
