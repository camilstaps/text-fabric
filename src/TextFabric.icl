implementation module TextFabric

/**
 * This file is part of text-fabric.
 *
 * Text-fabric is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Text-fabric is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with text-fabric. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdOverloadedList

import Control.Applicative
import Control.Monad
import Data.Functor

get_node_feature_id :: !String !DataSet -> ?FeatureId
get_node_feature_id name data =
	case [i \\ n <-: data.node_features & i <- [0..] | n==name] of
		[i] -> ?Just i
		_   -> ?None

get_node_feature_ids :: ![String] !DataSet -> ?[FeatureId]
get_node_feature_ids names data = mapM (flip get_node_feature_id data) names

get_edge_feature_id :: !String !DataSet -> ?FeatureId
get_edge_feature_id name data =
	case [i \\ n <-: data.edge_features & i <- [0..] | n==name] of
		[i] -> ?Just i
		_   -> ?None

get_edge_feature_ids :: ![String] !DataSet -> ?[FeatureId]
get_edge_feature_ids names data = mapM (flip get_edge_feature_id data) names

get_node_feature :: !FeatureId !Node -> String
get_node_feature i node = node.features.[i]

filter_nodes :: !NodeFilter !DataSet -> NodeList
filter_nodes p data = get_nodes (filter_node_refs p data) data

filter_node_refs :: !NodeFilter !DataSet -> NodeRefList
filter_node_refs p data=:{nodes} = [|i \\ n <-: nodes & i <- [0..] | p i n data]

get_nodes :: !NodeRefList !DataSet -> NodeList
get_nodes [|]     _    = [|]
get_nodes [|n:ns] data = [|data.nodes.[n]:get_nodes ns data]

/* NB: In the BHSA data, nodes can only be parent of word; never of other
 * types. Hence if we want to know if a clause is an ancestor of a phrase, we
 * need to look up all the words in the phrase and check that the clause is a
 * parent of all those words. */
is_ancestor_of :: !NodeRef !NodeRef !DataSet -> Bool
is_ancestor_of descendant ancestor data=:{nodes}
	# children = nodes.[descendant].outgoing.[0]
	/* If there are no children, the descendant is just a word and we need
	 * to check only that node. */
	# children = if (size children==0) {descendant} children
	= all_member_of children nodes.[ancestor].outgoing.[0]

all_member_of :: !{32#Int} !{32#Int} -> Bool
all_member_of needles haystack = check 0 needles 0 haystack
where
	check :: !Int !{32#Int} !Int !{32#Int} -> Bool
	check ni needles hi haystack
		| ni >= size needles = True
		| hi >= size haystack = False
		# c = needles.[ni]
		# e = haystack.[hi]
		| c < e     = False
		| c > e     = check ni     needles (hi+1) haystack
		| otherwise = check (ni+1) needles (hi+1) haystack

get_ancestor_nodes_with :: !NodeFilter !NodeRef !DataSet -> NodeList
get_ancestor_nodes_with p n data =
	[|data.nodes.[i] \\ i <|- get_ancestor_node_refs_with p n data]

get_ancestor_node_refs_with :: !NodeFilter !NodeRef !DataSet -> NodeRefList
get_ancestor_node_refs_with p n data=:{nodes}
	# children = nodes.[n].outgoing.[0]
	/* See comments on is_ancestor_of */
	# children = if (size children==0) {n} children
	/* TODO: it may be more efficient to
	 *   first collect all incoming edges
	 *   then check for each if they should be included
	 *   then sort
	 *   this way we can avoid the removeDup */
	= [|n \\ n <- sort (removeDup (collect (size children-1) children []))]
	with
		collect :: !Int !{32#NodeRef} ![NodeRef] -> [NodeRef]
		collect i children collected
			| i < 0 = collected
			# incoming = nodes.[children.[i]].incoming.[0]
			# collected = check (size incoming-1) incoming collected
			= collect (i-1) children collected
		where
			/* For each possible parent, check that all children of the
			 * start node are children of that parent. */
			check :: !Int !{32#NodeRef} ![NodeRef] -> [NodeRef]
			check -1 _ collected = collected
			check i parents collected
				# parent_ref = parents.[i]
				# parent = nodes.[parent_ref]
				# collected = if
					(p parent_ref parent data && all_member_of children parent.outgoing.[0])
					[parent_ref:collected]
					collected
				= check (i-1) parents collected

get_first_ancestor_node_with :: !NodeFilter !NodeRef !DataSet -> ?Node
get_first_ancestor_node_with p n data = case get_ancestor_nodes_with p n data of
	[|n:_]
		-> ?Just n
		-> ?None

get_first_ancestor_node_ref_with :: !NodeFilter !NodeRef !DataSet -> ?NodeRef
get_first_ancestor_node_ref_with p n data = case get_ancestor_node_refs_with p n data of
	[|r:_]
		-> ?Just r
		-> ?None

get_child_nodes_with :: !NodeFilter !Node !DataSet -> NodeList
get_child_nodes_with p n data =
	[|data.nodes.[i] \\ i <|- get_child_node_refs_with p n data]

get_child_node_refs_with :: !NodeFilter !Node !DataSet -> NodeRefList
get_child_node_refs_with p n data=:{nodes} = [|i \\ i <-: n.outgoing.[0] | p i nodes.[i] data]

is_descendant_of :: !NodeRef !NodeRef !DataSet -> Bool
is_descendant_of ancestor descendant data = is_ancestor_of descendant ancestor data

/* NB: see comments on is_ancestor_of: we make use of the knowledge that nodes
 * can only be the parent of word nodes. */
get_descendant_node_refs_with :: !NodeFilter !NodeRef !DataSet -> NodeRefList
get_descendant_node_refs_with p n data=:{nodes}
	# children = nodes.[n].outgoing.[0]
	# incoming =
		[ e
		\\ i <-: children
		 , e <-: nodes.[i].incoming.[0]
		 ]
	# descendants =
		[ descendant_ref
		\\ descendant_ref <- removeDup incoming
		 , let descendant = nodes.[descendant_ref]
		 | p descendant_ref descendant data && all_member_of descendant.outgoing.[0] children
		 ] ++
		[c \\ c <-: children | p c nodes.[c] data]
	= [|n \\ n <- sort descendants]

get_node_refs_with_distance_between_with :: !NodeFilter !Int !Int !NodeRef !DataSet -> NodeRefList
get_node_refs_with_distance_between_with p fr to ref data
	# slots = get_child_node_refs_with (\_ _ _ -> True) data.nodes.[ref] data
	# (start,end) = if (IsEmpty slots) (ref,ref) (Hd slots,Last slots)
	# matching_slots = find_matching_slots fr to start end
	# ancestors =
		[ slot \\ slot <|- matching_slots
		| p slot data.nodes.[slot] data
		] ++
		[ ancestor
		\\ slot <|- matching_slots
		 , ancestor <|- get_ancestor_node_refs_with p slot data
		 ]
	= [| ancestor \\ ancestor <- sort (removeDup ancestors)]
where
	find_matching_slots fr to start end =
		[if (fr<0) (start+fr) (end+fr) + 1 .. if (to<0) (start+to) (end+to) - 1]
