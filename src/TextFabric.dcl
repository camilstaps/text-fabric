definition module TextFabric

/**
 * This file is part of text-fabric.
 *
 * Text-fabric is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Text-fabric is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with text-fabric. If not, see <https://www.gnu.org/licenses/>.
 */

:: EdgeSet :== {32#Int}

:: Node` edges =
	{ features :: !.{#String}
	, outgoing :: !.{#edges}
	, incoming :: !.{#edges}
	}
:: Node :== Node` EdgeSet

:: DataSet` edges =
	{ node_features :: !.{#String}
	, edge_features :: !.{#String}
	, nodes         :: !.{#.Node` edges}
	}
:: DataSet :== DataSet` EdgeSet

:: NodeRef :== Int
:: NodeRefList :== [#NodeRef]
:: NodeList :== [!Node]

:: FeatureId :== Int

:: NodeFilter :== NodeRef Node DataSet -> Bool

get_node_feature_id :: !String !DataSet -> ?FeatureId
get_node_feature_ids :: ![String] !DataSet -> ?[FeatureId]

get_edge_feature_id :: !String !DataSet -> ?FeatureId
get_edge_feature_ids :: ![String] !DataSet -> ?[FeatureId]

get_node_feature :: !FeatureId !Node -> String

filter_nodes :: !NodeFilter !DataSet -> NodeList
filter_node_refs :: !NodeFilter !DataSet -> NodeRefList
get_nodes :: !NodeRefList !DataSet -> NodeList

is_ancestor_of :: !NodeRef !NodeRef !DataSet -> Bool

get_ancestor_nodes_with :: !NodeFilter !NodeRef !DataSet -> NodeList
get_ancestor_node_refs_with :: !NodeFilter !NodeRef !DataSet -> NodeRefList

get_first_ancestor_node_with :: !NodeFilter !NodeRef !DataSet -> ?Node
get_first_ancestor_node_ref_with :: !NodeFilter !NodeRef !DataSet -> ?NodeRef

get_child_nodes_with :: !NodeFilter !Node !DataSet -> NodeList
get_child_node_refs_with :: !NodeFilter !Node !DataSet -> NodeRefList

is_descendant_of :: !NodeRef !NodeRef !DataSet -> Bool
get_descendant_node_refs_with :: !NodeFilter !NodeRef !DataSet -> NodeRefList

/**
 * This finds the slots for which the distance to the specified node ref lies
 * within the specified interval (the limits are exclusive), and then returns
 * all their ancestors for which the filter holds.
 *
 * The distance of slots *before* the specified node ref is negative.
 *
 * If the node ref is not a single slot, the distance of slots *before* the ref
 * is measured to its first slot but the distance of slots *after* the ref is
 * measured from its last slot.
 */
get_node_refs_with_distance_between_with :: !NodeFilter !Int !Int !NodeRef !DataSet -> NodeRefList
