# Changelog

#### v0.2.2

- Enhancement: improve after-import memory usage by reusing edge sets.
- Fix: fix segmentation fault due to unsafe coercions in import, triggered by
  changes to base.
- Chore: allow base 3.0.

#### v0.2.1

- Chore: allow containers 2, system 2, and text 2.

## v0.2

- Chore: update to base 2.0.

#### v0.1.2

- Fix: fix `get_node_refs_with_distance_between_with`: only return slots when
  the predicate matches.

#### v0.1.1

- Chore: move to https://clean-lang.org.

## v0.1

First tagged version.
