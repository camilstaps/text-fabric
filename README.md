# Text-fabric

A library to manipulate [Text-Fabric][] files in [Clean][].

## Author & License
This library is written, maintained, and copyright &copy; by [Camil Staps][].

This project is licensed under AGPL v3; see the [LICENSE](/LICENSE) file.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[Text-Fabric]: https://github.com/annotation/text-fabric
